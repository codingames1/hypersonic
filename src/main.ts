import {Game} from "./models/Game";
import {debug} from "./services/debug";

const game = new Game();

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 *
 */

// @ts-ignore
const inputs = readline().split(' ');
const width = parseInt(inputs[0], 10);
const height = parseInt(inputs[1], 10);
const myId = parseInt(inputs[2], 10);

let tour = 0;

// game loop
while (true) {
    game.setTour(tour++);

    for (let i = 0; i < height; i++) {
        // @ts-ignore
        const row = readline();
        game.parseBox(i, row);
    }
    // @ts-ignore
    const entities = parseInt(readline(), 10);
    for (let i = 0; i < entities; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const entityType = parseInt(inputs[0], 10);
        const owner = parseInt(inputs[1], 10);
        const x = parseInt(inputs[2], 10);
        const y = parseInt(inputs[3], 10);
        const param1 = parseInt(inputs[4], 10);
        const param2 = parseInt(inputs[5], 10);

        game.addEntity({
            entityType,
            owner,
            x,
            y,
            param1,
            param2
        });
    }

    /*
     * Write an action using console.log()
     * To debug: console.error('Debug messages...');
     */

    console.log(game.nextAction());
}
