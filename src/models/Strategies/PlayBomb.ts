import {Strategy} from "./Strategy";
import {Point} from "../Point";

export class PlayBomb extends Strategy {

    protected actionName(): string {
        return "BOMB";
    }

    protected pointToMove(): Point {
        for (let i = 0; i < 10; i++) {
            const row = this.boxes[i];
            for (let j = 0; j < row.length; j++) {
                const elem = row[j];
                if(elem) {
                    return new Point({x:j, y:i});
                }
            }
        }
        return new Point({x:0, y:0});
    }

}
