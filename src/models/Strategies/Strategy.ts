import {Point} from "../Point";

export abstract class Strategy {

    protected boxes: any;

    protected abstract pointToMove(): Point;

    protected abstract actionName(): string;

    public nextAction(): string {
        const pointToMove = this.pointToMove();

        return `${this.actionName()} ${pointToMove.x} ${pointToMove.y}`;
    }

    constructor({boxes}: {boxes: any}) {
        this.boxes = boxes;
    }

}
