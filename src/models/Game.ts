import {Strategy} from "./Strategies/Strategy";
import {PlayBomb} from "./Strategies/PlayBomb";
import {PlayMove} from "./Strategies/PlayMove";
import {Player} from "./Entities/Player";
import {Bomb} from "./Entities/Bomb";

export class Game {

    public bombs: Array<Bomb> = [];
    public players: Array<Player> = [];
    public boxes: any = {};
    public tour: number = 0;

    public setTour(tour): void {
        this.tour = tour;
    }

    public nextAction(): string {

        const params = {boxes :this.boxes, tour: this.tour};

        let strategy: Strategy;
        if(this.moveStrategy()) {
            strategy = new PlayMove(params);
        }else{
            strategy = new PlayBomb(params);
        }

        return strategy.nextAction();
    }

    private moveStrategy(): boolean {
        return this.tour % 2 === 0;
    }

    public addEntity({ owner, entityType, x, y, param1, param2 }:{ owner: number; entityType: number; x: number; y: number; param1: number; param2: number }) {
        if(entityType) {
            this.bombs.push(new Bomb({x, y}, param1, param2, owner));
        }else {
            this.players.push(new Player({x, y}, param1, param2));
        }
    }

    public parseBox(i: number, row: string) {
        let rowFormated: any = row.split("");
        rowFormated = rowFormated.map(el => {
            return el === "0";
        });
        this.boxes[i] = rowFormated;
    }
}
