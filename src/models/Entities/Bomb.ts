import {Entity} from "./Entity";
import {Point} from "../Point";

export class Bomb extends Entity {

    public roundRemaining: number;
    public scope: number;
    public owner: number;

    constructor(coord: Point, roundRemaining: number, scope:number, owner: number) {
        super(coord);
        this.roundRemaining = roundRemaining;
        this.scope = scope;
        this.owner = owner;
    }

}
