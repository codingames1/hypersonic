import {Entity} from "./Entity";
import {Point} from "../Point";

export class Player extends Entity {

    public nbBombs: number;
    public scope: number;

    constructor(coord: Point, nbBombs: number, scope:number) {
        super(coord);
        this.nbBombs = nbBombs;
        this.scope = scope;
    }

}
