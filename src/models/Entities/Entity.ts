import {Point} from "../Point";

export abstract class Entity {
    public coord: Point;

    protected constructor(coord: Point) {
        this.coord = coord;
    }

}
